# Power Systems

A repo for stuff and related projects about Power Systems.

## Books and Articles

- Power Grid Failures: Theory of Fluctuating Renewables
- Day Ahead Load Forecasting for the Modern Distribution Network - A Tasmanian Case Study

## Related Projects

### Python

#### Libraries

- [PyPSA: Python for Power System Analysis](https://github.com/PyPSA/PyPSA)
- [pandapower: Convenient Power System Modelling and Analysis based on PYPOWER and pandas](https://github.com/e2nIEE/pandapower)
- [pandapipes: A pipeflow calculation tool that complements pandapower in the simulation of multi energy grids](https://github.com/e2nIEE/pandapipes)

#### Programs

- [GridCal: a power systems solver](https://gitlab.com/GridCal/GridCal)

## Other

- [Grid Map](https://www.entsoe.eu/data/map/)

## Academia

- [Articles which cite PyPSA](https://scholar.google.com/scholar?hl=en&as_sdt=2005&sciodt=0,5&cites=11241966939032736670&scipsc=&q=&scisbd=1)
- [Articles which cite pandapower](https://scholar.google.com/scholar?cites=5350289139775524430&as_sdt=2005&sciodt=0,5&hl=en)
